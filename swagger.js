const swaggerAutogen = require('swagger-autogen')({ language: 'pt-BR' })

const outputFile = './swagger_output.json'
const endpointsFiles = ['./src/controllers/AtendimentoController.js']

const doc = {
    info: {
        version: "1.0.0",
        title: "MY API NodeJS",
        description: "Documentação gerada automaticamente pelo módulo <b> swagger.autogen </b>"
    },
    host: "localhost:3000",
    basePath: "/",
    schemes: ['http', 'https'],
    consumes: ['application/json'],
    produces: ['application/json'],
    definitions: {
        FormAtendimento:{
            $cliente: "Cpf do cliente : Format String",
            pet: "Nome do pet : Format String",
            servico: "Nome do servico : Format String",
            data: "Data do agendamento : Format DD/MM/YYYY",
            status: "Status do agendamento : Format String",
            observacoes: "Obs do agendamento : Format String"
        },
        Atendimento: {
            id: 1,
            $cliente: "Cpf do cliente : Format String",
            pet: "Nome do pet : Format String",
            servico: "Nome do servico : Format String",
            data: "Data do agendamento : Format DD/MM/YYYY",
            dataCriacao: "Data de criação : Format 2021-02-10T16:02:42.000Z",
            status: "Status do agendamento : Format String",
            observacoes: "Obs do agendamento : Format String"
        },
        ListAtendimento: [
            { $ref: "#/definitions/Atendimento" }
        ]
    }
}

swaggerAutogen(outputFile, endpointsFiles, doc).then(() => {
    require('./index.js')
})