const { json } = require('body-parser')
const moment = require('moment')
const { lista } = require('../implements/AtendimentoImplement')
const AtendimentoService = require('../implements/AtendimentoImplement')
const Atendimento = require('../models/Atendimento')

class AtendimentoImplement {

    create(form) {

        const dataCriacao = moment().format('YYYY-MM-DD HH:MM:SS')
        const data = moment(form.data, 'DD/MM/YYYY').format('YYYY-MM-DD HH:MM:SS')

        const parametros = {
            data: { data, dataCriacao},
            cliente: { tamanho: form.cliente.length }
        }

        const atendimentoDatado = {...form, dataCriacao, data}

        return Atendimento.create(atendimentoDatado)
    }

    update(form){

        const data = moment(form.data, 'DD/MM/YYYY').format('YYYY-MM-DD HH:MM:SS')

        const atendimentoDatado = {...form, data}

        return Atendimento.update(atendimentoDatado, { returning: true, where: { id: form.id }})
    }

    findAll(){

        return Atendimento.findAll({})
    }

    findById(id){

        return Atendimento.findOne({where: { id: id }})
    }

    deleteById(id){
        
        return Atendimento.destroy({where: { id: id }})
    }

}

module.exports = new AtendimentoImplement