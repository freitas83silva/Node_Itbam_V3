const Sequelize = require('sequelize')
const database = require('../../config/DataSource')
 
const Atendimento = database.define('atendimento', {
    id: {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        allowNull: false,
        primaryKey: true
    },
    cliente: {
        type: Sequelize.STRING,
        allowNull: false
    },
    pet: {
        type: Sequelize.STRING,
        allowNull: false
    },
    servico: {
        type: Sequelize.STRING,
        allowNull: false
    },
    data: {
        type: Sequelize.STRING,
        allowNull: false
    },
    dataCriacao: {
        type: Sequelize.STRING,
        allowNull: false
    },
    status: {
        type: Sequelize.STRING,
        allowNull: false
    },
    observacoes: {
        type: Sequelize.STRING,
        allowNull: false
    }
},{
    timestamps: false
})
 
module.exports = Atendimento;