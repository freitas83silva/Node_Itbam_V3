//const { lista } = require('../implements/AtendimentoService')
const AtendimentoService = require('../implements/AtendimentoImplement')

module.exports = app => {

    app.post('/atendimento', (req, res) =>{
        /* #swagger.tags = ['Atendimento-controller']
           #swagger.summary = 'Create New Atendimento'
           #swagger.description = 'Endpoint para adicionar um atendimento.'
           #swagger.parameters['FormAtendimento'] = {
               in: 'body',
               description: 'Informações do atendimento.',
               required: true,
               type: 'object',
               schema: { $ref: "#/definitions/FormAtendimento" }
        } */
        const form = req.body

        AtendimentoService.create(form)

        /* #swagger.responses[201] = { 
            description: 'Atendimento registrado  com sucesso.',
            schema: { $ref: "#/definitions/Atendimento" }            
        } */
        .then(data => { res.send(data)})

        /* #swagger.responses[500] */
        .catch(err => { res.status(500).send({
            message:
              err.message || "Ocorreu algum erro ao criar o Atendimento."
          })
        })
    })
    
    app.put('/atendimento', (req, res) => {
        /* #swagger.tags = ['Atendimento-controller']
           #swagger.summary = 'Update Atendimento'
           #swagger.parameters['Atendimento'] = {
               in: 'body',
               description: 'Informações do atendimento.',
               required: true,
               type: 'object',
               schema: { $ref: "#/definitions/Atendimento" }
        } */
        const form = req.body

        AtendimentoService.update(form)

        /* #swagger.responses[200] = {
            description: 'Atendimento atualizado com sucesso.', 
            schema: { $ref: "#/definitions/Atendimento" }            
        } */
        .then(data => { res.send(data)})
   
        /* #swagger.responses[500] */
        .catch(err => { res.status(500).send({
            message:
              err.message || "Ocorreu algum erro ao atualizar o Atendimento."
          })
        })
    })

    app.get('/atendimento', (req, res) => {
        /* #swagger.tags = ['Atendimento-controller']
           #swagger.summary = 'Get All todos os Atendimentos' */
        AtendimentoService.findAll()

        /* #swagger.responses[200] = { 
            description: 'Lista de Atendimentos.',
            schema: { $ref: "#/definitions/ListAtendimento" }
        } */
        .then(data => { res.send(data)})

        /* #swagger.responses[500] */
        .catch(err => { res.status(500).send({
            message:
              err.message || "Ocorreu algum erro ao recuperar os Atendimentos."
          })
        })
     })

    app.get('/atendimento/:id', (req, res) => {
	      /* #swagger.tags = ['Atendimento-controller']
           #swagger.summary = 'Get Atendimento pelo id'
           #swagger.description = 'Endpoint para obter um atendimento.'
           #swagger.parameters['id'] = { 
              type: 'integer',
              description: 'Id do Atendimento.' }*/
        
        const id = parseInt(req.params.id)

        AtendimentoService.findById(id)

        /* #swagger.responses[200] = { 
            description: 'Atendimento encontrado.', 
            schema: { $ref: "#/definitions/Atendimento" }
        } */
        .then(data => { res.send(data)})

        /* #swagger.responses[500] */
        .catch(err => { res.status(500).send({
            message: "Erro ao recuperar o atendimento com id =" + id
          })
        })
    })
    
    app.delete('/atendimento/:id', (req, res) => {
        /* #swagger.tags = ['Atendimento-controller']
           #swagger.summary = 'Remove Atendimento pelo id'
           #swagger.description = 'Endpoint para deletar um atendimento.'
           #swagger.parameters['id'] = { 
              type: 'integer',
              description: 'Id do Atendimento.' }*/
        const id = parseInt(req.params.id)

        AtendimentoService.deleteById(id)

        /* #swagger.responses[200] */
        .then(num => {
            if (num == 1) {
              return res.send({ message: "Atendimento excluído com sucesso!" })
            } else {
              return res.send({ message: `Não é possível excluir o Atendimento com id = ${id}. Talvez o atendimento não tenha sido encontrado!` })
            }
          })

        /* #swagger.responses[500] */
        .catch(err => { res.status(500).send({
            message: "Não foi possível excluir o atendimento com id =" + id
          })
        })
    })
}