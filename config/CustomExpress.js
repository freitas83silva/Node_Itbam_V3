const express = require('express')
const consign = require('consign')
const bodyParser = require('body-parser')
const swaggerUi = require('swagger-ui-express')
const swaggerFile = require('../swagger_output.json')

module.exports = () => {
    const app = express()

    app.use(bodyParser.urlencoded({ extended: true}))
    app.use(bodyParser.json())

    app.use('/api', swaggerUi.serve, swaggerUi.setup(swaggerFile))

    consign()
        .include('src/controllers')
        .into(app)   

    return app
}