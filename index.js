const customExpress = require('./config/CustomExpress')
const connection = require('./config/DataSource')

const conexao = connection.authenticate()
.then(function(){
   const app = customExpress()
   app.listen(3000, () => console.log('Sucesso: Servidor rodando na porta 3000'))
})
.catch(function (err) {
  console.log('Erro: Não foi possível se conectar com o banco de dados MySql',err)
})
.done()
